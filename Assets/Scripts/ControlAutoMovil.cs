using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlAutoMovil : MonoBehaviour
{
    float velocidad = 30.0f;
    float velocidadGiro=35;
    float mandoHorizontal;
    float mandoVertical;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //variables de Mando
        mandoHorizontal = Input.GetAxis("Horizontal");
        mandoVertical = Input.GetAxis("Vertical");

        //Mando de avance
        transform.Translate(Vector3.forward * Time.deltaTime * velocidad* mandoVertical);

        //mando de Giro
        transform.Rotate(Vector3.up* Time.deltaTime * velocidadGiro * mandoHorizontal);
    }
}
